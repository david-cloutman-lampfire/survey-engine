import React, { Component } from 'react';
import SurveyResults from './SurveyResults';
import {survey} from '../models';

/**
 * This component displays the survey as a questionnaire.
 */
class SurveyQuestionnaire extends Component {
    constructor (props) {
        super(props);
        this.state = {
            survey: survey
        };
    }

    displayResults = event => {
        this.props.changeMainDisplay(<SurveyResults />)
    }

    render () {
        return (
            <div>
                <h1>Stop! Survey Time!</h1>
                <ol className="questionList">
                    {survey.questions.map(q => (
                        <li key={q.id}><h3>{q.questionText}</h3></li>
                    ))}
                </ol>
                <button onClick={this.displayResults}>Display Results</button>
            </div>
        );
    }
}

/*class SurveyQuestion extends Component {
    render () {
        return (
        );
    }
}*/

export default SurveyQuestionnaire;
