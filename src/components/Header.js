import React, { Component } from 'react';

// From http://logomakr.com/1PFfQl
import logo from '../wl-logo.png';


/**
 * Renders the page's <header /> tag.
 */
class Header extends Component {
    render() {
        
        return (
            <header className="App-header container-fluid">
                <div className="row">
                    <div className="col-xs-1"></div>
                    <div className="col-xs-2">
                        <img src={logo} className="App-logo" alt="logo" />
                    </div>
                    <div className="col-xs-6">
                        <h2>Simple Survey Engine</h2>
                    </div>
                    <div className="col-xs-3"></div>
                </div>
            </header>
        );
    }
}

export default Header;
