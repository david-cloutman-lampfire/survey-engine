import React, { Component } from 'react';
import SurveyQuestionnaire from './SurveyQuestionnaire';
import {survey, SingleResponseQuestion, MultiResonseQuestion, TextResponseQuestion} from '../models';

/**
 * This component allows the user to add and configure questions in the survery.
 */
class SurveyBuilder extends Component {
    constructor (props) {
        super(props);
        this.state = {
            survey: survey,
            questionBuilders: []
        };
    }

    configureSurvey = event => {
        event.preventDefault();

        // console.log(this.state.questionBuilders);
        // Build out the survey model from the form data.
        /*for(let i = 0; i < this.state.questionBuilders.length; i++) {
            console.log(this.state.questionBuilders[i]);
            this.state.questionBuilders[i].addToSurvey();
            //console.log(builder);
        }*/

        this.props.changeMainDisplay(<SurveyQuestionnaire changeMainDisplay={this.props.changeMainDisplay} />);
    }

    addQuestion = (event, type) => {
        event.preventDefault();

        let uid = this.state.questionBuilders.length;
        //console.log(uid);
        this.setState({questionBuilders: [...this.state.questionBuilders, <QuestionBuilder uid={uid} type={type} />]});
        //console.log(this.state.questionBuilders.length);
    }

    addTextQuestion = (event) => {
        this.addQuestion(event, 'text')
    }

    addMultipleAnswerQuestion = (event) => {
        this.addQuestion(event, 'multi')
    }

    addSingleAnswerQuestion = (event) => {
        this.addQuestion(event, 'single')
    }

    render () {
        return (
            <form onSubmit={this.configureSurvey}>
                <h1>Create Your Survey</h1>
                <p>
                    You may add as many questions as you like.
                </p>
                <ol className="questionList" id="questionList">{this.state.questionBuilders.map(builder => (
                    <li
                        key={builder.props.uid}
                        id={`questionBuilder_${builder.props.uid}`}>
                            {builder}
                    </li>
                ))}
                </ol>
                <button onClick={this.addTextQuestion} className="small blue">Add Text Question</button>
                <button onClick={this.addSingleAnswerQuestion} className="small green">Add Single Answer Question</button>
                <button onClick={this.addMultipleAnswerQuestion} className="small purple">Add Multiple Answer Question</button>

                <hr />
                <input type="submit" value="Save and Take Survey" />
            </form>
        );
    }
}

class QuestionBuilder extends Component {
    /**
     * Takes props [type, uid].
     */
    constructor (props) {
        super(props);
        this.uid = props.uid;

        let q = null; // Reference to the Question.

        if ('single' === props.type) {
            q = new SingleResponseQuestion('');
        }
        else if ('multi' === props.type) {
            q = new MultiResonseQuestion('');
        }
        else if ('text' === props.type) {
            q = new TextResponseQuestion('');
        }
        else {
            throw new Error("Invalid question type.");
        }

        survey.addQuestion(q);

        this.state = {
            type: props.type,
            responseQuestion: q
        };

    }

    setQuestionText = event => {
        let q = this.state.responseQuestion;
        q.questionText = event.target.value;
        this.setState({responseQuestion: q});
    }

    addOption = event => {
        event.preventDefault();
        this.state.responseQuestion.addOption('');
    }

    render () {
        let options = '';
        if('single' === this.props.type || 'multi' === this.props.type) {
            options = (
                <div className={`${this.props.type}Options`}>
                    <ul>
                        {this.state.responseQuestion.options.map(option => (
                            <OptionBuilder key={Date.now()} type={this.props.type} />
                        ))}
                    </ul>
                    <button className="tiny grey" onClick={this.addOption}>Add Option</button>
                </div>
            );
        }

        return (
            <div>
                <label>Question Text</label><br />
                <input
                    type="text"
                    className="QuestionBuilder-questionTextInput"
                    onChange={this.setQuestionText}
                    value={this.state.responseQuestion.questionText} />

                {options}
            </div>
        );
    }
}

class OptionBuilder extends Component {
    constructor (props) {
        super(props);
        this.state = props.question;
    }
}

export default SurveyBuilder;
