/**
 * A base class for survey questions.
 */
class SurveyQuestion {
    constructor (questionText) {
        this.questionText = questionText;
    }

    /** Sets a default behavior. Inheriting classes should override this method.
     */
    getResponse = () => {
        return '';
    }
}

/**
 * Represents a free text question where the response is entered by
 * the user.
 */
export class TextResponseQuestion extends SurveyQuestion {
    constructor (questionText) {
        super(questionText);
        this.response = '';
    }

    getResponse = () => {
        return this.response;
    }
}

/**
 * An abstract class for questions that have pre-defined responses that a 
 * selects from.
 */
export class FixedResponsesQuestion extends SurveyQuestion {
    constructor (questionText) {
        super(questionText);

        this.options = [];
    }

    addOption (newOption) {
        this.options.push(newOption);
    }

    getPossibleResponses () {
        return this.option;
    }
}

/**
 * Represents a question with pre-defined responses where a user may
 * select more than one response.
 */
export class MultiResonseQuestion extends FixedResponsesQuestion {
    constructor (questionText) {
        super(questionText);

        // An array of numeric indexed for responses selected from this.option.
        this.responseIndexes = [];
    }

    /**
     * Returns an array of strings.
     */
    getResponse = () => {
        let responses = [];

        // TODO: Add a loop that iterated over responseIndexes and a
        let len = this.responseIndexes.length;
        for (let i = 0; i < len; i++) {
            responses.push(this.option[i]);
        }

        return responses;
    }
}

/**
 * Represents a question with pre-defined responses where a user may
 * select one and only one response.
 */
export class SingleResponseQuestion extends FixedResponsesQuestion {
    constructor (questionText) {
        super(questionText);

        this.responseIndex = null;
    }
}

export class Survey {
    constructor () {
        this.nextId = 0;
        this.questions = [];
    }

    addQuestion = (question) => {
        question.id = this.nextId;
        this.questions.push(question);
        this.nextId++;
    }
}

// Instantiate the single instance of Survey in the application.
export let survey = new Survey();
