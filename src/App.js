import React, { Component } from 'react';
import './App.css';
import Header from './components/Header';
import SurveyBuilder from './components/SurveyBuilder';


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mainDisplay: <SurveyBuilder changeMainDisplay={this.changeMainDisplay} />
        };
    }

    changeMainDisplay = component => {
        this.setState({mainDisplay: component});
    }

    render() {
        return (
            <div className="App">
                <Header caller={this} />
                <main id="mainDisplay" className="container-fluid">
                    <div className="row">
                        <div className="col-xs-1"></div>
                        <div className="col-xs-10">
                            {this.state.mainDisplay}
                        </div>
                        <div className="col-xs-1"></div>
                    </div>
                </main>
            </div>
        );
    }
}

export default App;
