import React, { Component } from 'react';
import {survey} from '../models';


/**
 * This component displays the survey as a set of results.
 */
class SurveyResults extends Component {
    constructor (props) {
        super(props);
        this.state = {
            survey: survey
        };

    }
    render () {
        return (
            <div>
                <h1>Survey Results</h1>
                <ol className="questionList">
                    {survey.questions.map(q => (
                        <li key={q.id}><h3>{q.questionText}</h3></li>
                    ))}
                </ol>
            </div>
        );
    }
}

export default SurveyResults;
